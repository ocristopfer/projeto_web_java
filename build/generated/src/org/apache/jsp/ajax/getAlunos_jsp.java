package org.apache.jsp.ajax;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.ArrayList;
import java.util.List;
import db.persistence.DbManager;
import java.sql.Connection;
import java.sql.DriverManager;
import db.persistence.Alunos;

public final class getAlunos_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");

    
        DbManager db = new DbManager();
        Connection conn = db.getConnection(); 
        Alunos oAluno = new Alunos(conn);
        
        List<Alunos> List_Alunos; 
        List_Alunos = oAluno.listarAlunos();
        String data = "";
        for(Alunos aluno: List_Alunos){
            data += " ["+
            "\""+aluno.id+"\", "+
            "\""+aluno.nome+"\", "+
            "\""+aluno.cpf+"\", "+
            "\""+aluno.rg+"\", "+
            "\""+aluno.sexo+"\", "+
            "\""+aluno.data_nascimento+"\", "+
            "\""+aluno.data_cadastro+"\", "+
            "\""+aluno.data_exclusão+"\", "+
            "],";
        }
        
  
        
   
        String json = "{"+
                "\"sEcho\": 1,"+
                "\"iTotalRecords\": "+List_Alunos.size() +","+
                "\"iTotalDisplayRecords\": 2,"+
                "\"aaData\": ["+
                data + 
                "]"+
                "}";
        
        out.print(json);


      out.write("\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
