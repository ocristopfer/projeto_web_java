/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cristopfer
 */
public class Alunos {
    
    public int id;
    public String nome;
    public String cpf;
    public String rg;
    public String sexo;
    public String data_nascimento;
    public String data_cadastro;
    public Date data_exclusão;
    public String acao;
    
    private Connection Conn;
    
    public Alunos(Connection Conn){
        this.Conn = Conn;
    }

    private Alunos() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public List<Alunos> listarAlunos(boolean excluido) throws SQLException{
      List<Alunos> List_Alunos = new ArrayList<Alunos>();  
      String query = "SELECT * FROM alunos";
      if(excluido){
            query = query + " WHERE data_exclusao is not null";
      }else{
          query = query + " WHERE data_exclusao is null";
      }
      
        // create the java statement
      Statement st = Conn.createStatement();
      
      // execute the query, and get a java resultset
      ResultSet rs = st.executeQuery(query);
      
      // iterate through the java resultset
      while (rs.next())
      {
       Alunos oAluno = new Alunos();
       oAluno.id = rs.getInt("id");
       oAluno.nome = rs.getString("nome");
       oAluno.cpf = rs.getString("cpf");
       oAluno.rg = rs.getString("rg");
       oAluno.sexo = rs.getString("sexo");
       
       
       SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
       //String strDate = formatter.format(rs.getDate("data_nascimento"));  
       
       oAluno.data_nascimento = formatter.format(rs.getDate("data_nascimento"));
       oAluno.data_cadastro = formatter.format(rs.getDate("data_cadastro"));
       oAluno.data_exclusão = rs.getDate("data_exclusao");     
       
       List_Alunos.add(oAluno);  
      }
   
      return List_Alunos;
    }
    
}
