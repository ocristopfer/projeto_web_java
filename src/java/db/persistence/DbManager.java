/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author 2015200611
 */
public class DbManager {
    public Connection getConnection(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/projeto_web", "root", "suporte");
             return conn;
        }catch(ClassNotFoundException e){
            return null;
        }catch(SQLException e){
            return null;
        }
    
    }
    
}
