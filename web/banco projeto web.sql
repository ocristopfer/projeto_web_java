-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.6.5-m8 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para projeto_web
CREATE DATABASE IF NOT EXISTS `projeto_web` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `projeto_web`;

-- Copiando estrutura para tabela projeto_web.alunos
CREATE TABLE IF NOT EXISTS `alunos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL DEFAULT '',
  `cpf` varchar(15) DEFAULT '',
  `rg` varchar(15) DEFAULT '',
  `sexo` char(1) DEFAULT '',
  `data_nascimento` datetime DEFAULT NULL,
  `data_cadastro` datetime DEFAULT NULL,
  `data_exclusao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela projeto_web.alunos: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `alunos` DISABLE KEYS */;
INSERT INTO `alunos` (`id`, `nome`, `cpf`, `rg`, `sexo`, `data_nascimento`, `data_cadastro`, `data_exclusao`) VALUES
	(1, 'ALuno1', '150444666555', '2469545546', 'F', '2000-08-21 16:35:43', '2019-08-21 16:35:48', NULL),
	(2, 'ALuno2', '150444666555', '2469545546', 'M', '2000-08-21 16:35:43', '2019-08-21 16:35:48', NULL);
/*!40000 ALTER TABLE `alunos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
