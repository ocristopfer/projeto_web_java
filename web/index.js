/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {            
        dataTable = $('#ListaAlunos').DataTable({
         responsive:false,
        dom: 'Bfrtip',
        "iDisplayLength": 20,
        "ordering": false,
        "bSort": false,
        "bFilter": false,
        'sAjaxSource': "getAlunos",
        "bLengthChange": false,
        'aoColumns': [
            { 'mData': 'id' },
            { 'mData': 'nome' },
            { 'mData': 'cpf' },
            { 'mData': 'rg' },
            { 'mData': 'sexo' },
            { 'mData': 'data_nascimento' },
            { 'mData': 'data_cadastro' } 
        ],
        "columnDefs": [ {
            "targets": 7,            
            "defaultContent": " <button type='button' class='btn btn-primary' data-dismiss='modal'>Editar</button>"
        } ],
        'oLanguage': {
            'oPaginate': {
                'sFirst': "Primeiro",
                'sLast': "Último",
                'sNext': "Próximo",
                'sPrevious': "Anterior",
                'sEmptyTable': "Nenhum resultado encontrado"
            },
            'sInfo': "Visualização do registro _START_ ao _END_ de _TOTAL_",
            'sInfoEmpty': "Visualização do registro 0 ao 0 de 0",
            'sLengthMenu': "Visualização de _MENU_ registros",
            'sLoadingRecords': "Carregando...",
            'sProcessing': "Processando...",
            'sZeroRecords': "Nenhum resultado encontrado"
        },
        "sPaginationType": "full_numbers"

     });  
     
       $('#ListaAlunos tbody').on('click', 'td', function () {
        if(dataTable.cell(this).index().column === 7){
        var data = dataTable.row(dataTable.cell(this).index().row).data();
        abriModal(data);
        }
    });
    
} );

function alerta(msg){
    bootbox.alert(msg);
}

function abriModal(data) {
    limparDados();
    if(data){
    $('#editarmodallabel').text('Editar Cadastro')
    $('#matricula').val(data.id)
    $('#nome').val(data.nome)
    $('#cpf').val(data.cpf)
    $('#rg').val(data.rg)
    $('#sexo').val(data.sexo)
    $('#data_nascimento').val(data.data_nascimento)
    }else{
        $('#editarmodallabel').text('Novo Cadastro')
    }
    
    $("#editarmodal").modal();    
}

function limparDados(){
    $('#matricula').val('')
    $('#nome').val('')
    $('#cpf').val('')
    $('#rg').val('')
    $('#sexo').val('')
    $('#data_nascimento').val('')
}

function salvar(){
        $.post("setAlunos", {
           'matricula': $('#matricula').val(),
           'nome': $('#nome').val(),
           'cpf': $('#cpf').val(),
           'rg': $('#rg').val(),
           'sexo' : $('#sexo').val(),
           'data_nascimento': $('#data_nascimento').val()
            }).done(function(data) {
                console.log(data);
            });
}

function excluir(parameters) {
    bootbox.confirm('Desejá realmente excluir ?', function(response){
        if(response){
            alert('ok')
        }else{
            alert('no')
        }
        
    })
}
 
