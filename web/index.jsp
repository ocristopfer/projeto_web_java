<%-- 
    Document   : index
    Created on : 20/08/2019, 19:34:59
    Author     : 2015200611
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import = "db.persistence.DbManager" %>
<%@page import = "java.sql.Connection" %>
<%@page import= "java.sql.DriverManager" %>
<%@page import= "db.persistence.Alunos" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro de Alunos</title>
               <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.css">
        <script src="plugins/jquery/jquery-3.4.1.min.js"></script>
        <script src="plugins/bootstrap/js/bootstrap.js"></script>
        <script src="plugins/bootbox/bootbox.min.js"></script>
        <script src="plugins/bootbox/bootbox.locales.min.js"></script>
        
        <link rel="stylesheet" type="text/css" href="plugins/DataTables/datatables.min.css"/>
        <script type="text/javascript" src="plugins/DataTables/datatables.min.js"></script>
        
        <script src="index.js?v2"></script>
    </head>
    <body>
         <%
             
        DbManager db = new DbManager();
        Connection conn = db.getConnection(); 
               
 
        if(conn == null){
        out.print("Erro ao tentar com banco de dados");
        }else{
        out.print("Conectado!");
        }
         %>
        
        <form>
        <br>
        <div style="align:center; ">           
            <table id="ListaAlunos" class="display">
             <thead>
                <tr>
                <th>Matricula</th>
                <th>Nome</th> 
                <th>CPF</th>
                <th>RG</th>
                <th>Sexo</th>
                <th>Data Nascimento</th>
                <th>Data Cadastro</th>
                <th>Ação</th>
                </tr>
             </thead>
            </table>           
        </div>
        
        <!-- Editar Modal -->
        <div class="modal fade" id="editarmodal" tabindex="-1" role="dialog" aria-labelledby="editarmodallabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title" id="editarmodallabel"> Cadastro</h3>
              </div>
              <div class="modal-body">
                 Matricula: <br>
                <input id="matricula" type="text"></input>
                 <br>
                 Nome: <br>
                <input id="nome" type="text"></input>
                
                <br>
                 CPF: <br>
                <input id="cpf" type="text"></input>
                               
                 <br>RG: <br> 
                <input id="rg" type="text"></input>
                
                <br>SEXO: <br>
                 <select id="sexo">
                    <option value="M">Masculino</option>
                    <option value="F">Feminino</option>
                  </select> 
                
                 <br>Data Nascimento: <br> 
                
                <input id="data_nascimento" data-provide="datepicker">
                
                  
              </div>
              <div class="modal-footer">
                <button type="button" onclick= "excluir('essa merda é só um teste!')" class="btn btn-primary">Excluir</button>
                <button type="button" onclick= "salvar()" class="btn btn-primary">Salvar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
              </div>
            </div>
          </div>
        </div>
       
       
        <br>
        <div style="text-align:center">
            <button type="button" onclick= "abriModal()" class="btn btn-primary">Cadastrar</button>
       
            </div>
        </form>
    </body>
   
</html>
